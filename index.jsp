<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css?version=2">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Пункт проката велосипедов</title>
    </head>
    <body>
        <header>
            <a href="/"><img alt="Логотип" id="top-image" src="https://avatars.mds.yandex.net/get-altay/1773749/2a0000016a6d89fbd19b5da8854fbba223b8/XXL"/></a>
            <div id="user-panel">
                <a href="#"><img alt="Фото велолюбителя" scr=""/></a>
                <a href="javascript:void(0);">Панель</a>
            </div>
        </header>
        <div id="main">
            <aside class="leftAside">
                    <a href="#">Директор велопроката "ПрокатТЛТ" - Богатов Евгений Александрович</a>
                    <a href="/"><img alt="" id="right-image" src="https://истории.рф/wp-content/uploads/2019/09/25-1-1024x748.jpg"/></a>
            </aside>
            <article class="RideAside">
                <ul>
                    <a href="#">О нас</a>
                    <a href="#">Фото велосипедов</a>
                    <a href="#">Контакты</a>
                    
                </ul>
            </article>
            <section>
                <article>
                    <h1>Дополнительные услуги теперь у нас в прокате ВелоТЛТ!</h1>
                    <div class="text-article">
                        Приветствую, дорогие велолюбители. Сегодня нам 1 месяц!!! Поэтому, в эти выходные действует 20% скидка на прокат абсолютно всех велосипедов. Спешите! Мы находисмся по адресу: г. Тольятти б-р Ленина д.1 (вход с торца). За дополнительной информацией обращайтесь по телефону +7(8482)22-99-22. 
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Автор статьи: <a href="#">администратор велопроката "ПрокатТЛТ"</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата статьи: 11.06.2021</span>
                    </div>
                </article>
                <article>
                    <h1>Мы открылись!</h1>
                    <div class="text-article">
                        Приветствуем любителей велоспорта, мы представляем Вам наш новый велопрокат "ВелоТЛТ", находящийся по адресу: г. Тольятти б-р Ленина д.1 (вход с торца). Мы представляем Вам данные виды услуг:
                        <ul>
                    <li>прокат взрослого велосипеда - 200р/час (сутки 1000р)</li>
                    <li>прокат детского велосипеда - 150р/час (сутки 700р)</li>
                    <li>прокат взрослого фэтбайка - 300р/час (сутки 1500р)</li>
                    <li>прокат детского фэтбайка - 200р/час (сутки 1000р)</li>
                    
                        </ul>
                        За дополнительной информацией обращайтесь по телефону +7(8482)22-99-22.
                        
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Автор статьи: <a href="#">администратор велопроката "ПрокатТЛТ"</a></span>
                        <span class="read"><a href="javascript:void(0);">Читать...</a></span>
                        <span class="date-article">Дата статьи: 11.05.2021</span>
                        
                    </div>
                </article>
            </section>
            <article>
                    <a href="#">Сообщить об ошибке</a>
                    <a href="#">Политика конфиденциальности</a>
            </article>
        </div>
        <footer>
            <div>
                <span>г. Тольятти б-р Ленина д.1 (вход с торца) +7(8482)22-99-22</span>
            </div>
        </footer>
    </body>
</html>