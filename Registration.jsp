<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css?version=2">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Пункт проката велосипедов</title>
    </head>
    <body>
        <header>
            <a href="/"><img alt="Логотип" id="top-image" src="https://avatars.mds.yandex.net/get-altay/1773749/2a0000016a6d89fbd19b5da8854fbba223b8/XXL"/></a>
            <div id="user-panel">
                <a href="#"><img alt="Фото велолюбителя" scr=""/></a>
                <a href="javascript:void(0);">Панель</a>
            </div>
        </header>
        <div id="main">
            <aside class="leftAside">
                <h2>Что нужно для регистрации</h2>
                <p>Что бы регистрация прошла успешно, заполните все поля и нажмите на
                кнопку "Зарегистрироваться"
                </p>
            </aside>
            <section>
                <article>
                    <h1>Регистрация</h1>
                    <div class="text-article">
                        <form method="POST" action="registration">
                        <p>
                            <label for="login">Логин</label>
                            <input type="text" name="login" id="login"/>
                        </p>
                        <p>
                        <label for="email">E-Mail</label>
                        <input type="email" name="email" id="email"/>
                        </p>
                        <p>
                        <label for="password">Пароль</label>
                        <input type="password" name="password" id="password"/>
                        
                        <label for="password2">Повторите пароль</label>
                        <input type="password" name="password2" id="password2"/>
                        </p>
                        <p>
                            <button type="submit">Зарегистрироваться</button>
                        </p>
                        </form>
                    </div>
                </article>
            </section>
        </div>
        <footer>
            <div>
                <span>г. Тольятти б-р Ленина д.1 (вход с торца) +7(8482)22-99-22</span>
            </div>
        </footer>
    </body>
</html>